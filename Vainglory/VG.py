import csv
import requests
from progress.bar import IncrementalBar


def heroes():
    herolist = []

    with open('heroes.txt', 'r+') as f:
        for line in f:
            herolist.append(line.strip())
        herolist.sort()
        f.seek(0)
        for line in herolist:
            f.write(f'{line}\n')

    bar = IncrementalBar('Reading Hero Data', max=len(herolist))

    for i, hero in enumerate(herolist):
        website = 'https://vgwiki.netlify.com/page-data/' + hero + '/page-data.json'
        response = requests.get(website)
        json_data = response.json()
        result = json_data['result']
        data = result['data']
        allJavascriptFrontmatter = data['allJavascriptFrontmatter']
        edges = allJavascriptFrontmatter['edges']
        item = edges[0]
        node = item['node']
        frontmatter = node['frontmatter']
        stats = frontmatter['stats']

        hero_data = {'Name': hero}
        for index, item in enumerate(stats):
            if index < 8:
                keys = [item['name'].title() + ' Level 1', item['name'].title() + ' Level 12']
                values = item['value'].split('-')
                if len(values) == 1:
                    values = ['0', '0']
                for k, value in enumerate(values):
                    value = value.strip()
                    value = value.replace('%', '')
                    if value == '':
                        value = 0
                    values[k] = float(value)
                hero_data.update({keys[0]: values[0], keys[1]: values[1]})
            else:
                value = item['value']
                if value == '-':
                    value = 0
                hero_data.update({item['name'].title(): value})
        herolist[i] = hero_data
        bar.next()

    with open('HeroInfo.csv', 'w+', newline='') as csv_file:
        fields = []
        for key in herolist[0].keys():
            fields.append(key)
        writer = csv.DictWriter(csv_file, fields)
        writer.writeheader()
        for hero in herolist:
            writer.writerow(hero)


def items():
    itemlist = []

    with open('items.txt', 'r+') as f:
        for line in f:
            itemlist.append(line.strip())
        itemlist.sort()
        f.seek(0)
        for line in itemlist:
            f.write(f'{line}\n')

    bar = IncrementalBar('Reading Item Data', max=len(itemlist))

    website = 'https://vgwiki.netlify.com/component---src-pages-index-js-e50f03e1e9e2d0dc40fd.js'

    response = requests.get(website)
    print(response.text)


if __name__ == '__main__':
    while True:
        print('1: Heroes')
        print('2: Items')
        print('3: exit')
        module = input()
        if module == '1':
            heroes()
        elif module == '2':
            items()
        elif module == '3':
            exit()
        print()
