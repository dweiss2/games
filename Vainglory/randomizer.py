import random

heroes = ['Adagio',
          'Alpha',
          'Anka',
          'Ardan',
          'Baptiste',
          'Baron',
          'Blackfeather',
          'Caine',
          'Catherine',
          'Celeste',
          'Churnwalker',
          'Flicker',
          'Fortress',
          'Glaive',
          'Grace',
          'Grumpjaw',
          'Gwen',
          'Idris',
          'Inara',
          'Ishtar',
          'Joule',
          'Kensei',
          'Kestrel',
          'Kinetic',
          'Koshka',
          'Krul',
          'Lance',
          'Leo',
          'Lorelai',
          'Lyra',
          'Magnus',
          'Malene',
          'Miho',
          'Ozo',
          'Petal',
          'Phinn',
          'Reim',
          'Reza',
          'Ringo',
          'Rona',
          'SAW',
          'Samuel',
          'SanFeng',
          'Silvernail',
          'Skaarf',
          'Skye',
          'Taka',
          'Tony',
          'Varya',
          'Viola',
          'Vox',
          'Warhawk',
          'Yates',
          'Ylva',
          ]

print('Enter "3" for 3v3 and "5" for 5v5.')
mode = int(input())
for side in ['A', 'B']:
    team = random.sample(heroes, 5)
    position = ['Top', 'Bot', 'Mid', 'Jungle', 'Captain'] if mode == 5 else\
               ['Lane', 'Jungle', 'Captain']
    random.shuffle(position)
    role = [random.choice(['WP', 'CP', 'Utility']) for _ in range(mode)]
    if 'Utility' not in role:
        cap = random.randint(0, 4)
        role[cap] = 'Utility'
    print(f'Side {side}:')
    [print(f'{role[i]} {position[i]} {team[i]}') for i in range(mode)]
    print()
