import os.path
import pickle

from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build

# collection = 'https://docs.google.com/spreadsheets/d/1ZlIL3fkb6090ERlOUR8h5AZ-0rYRJXDGId8jZwUhWq0'

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']

# The ID and range of a sample spreadsheet.
workbook_id = '1ZlIL3fkb6090ERlOUR8h5AZ-0rYRJXDGId8jZwUhWq0'
sheet_names = ['Summary', 'CardList', 'Open Pack Stats', 'Open Pack Analysis', 'Reference']


def get_data():
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets().values()
    cards = sheet.get(spreadsheetId=workbook_id, range=f'{sheet_names[1]}!B:U').execute().get('values', [])
    open_packs = sheet.get(spreadsheetId=workbook_id, range=f'{sheet_names[2]}!C20:G1090').execute().get('values', [])
    pack_counts = sheet.get(spreadsheetId=workbook_id, range=f'{sheet_names[2]}!C4:D15').execute().get('values', [])
    sets = sheet.get(spreadsheetId=workbook_id, range=f'{sheet_names[4]}!J4:K22').execute().get('values', [])
    openable = [set[0] for set in sets if set[1] == 'Expansion' or set[0] == 'Classic']
    cards = [dict(zip(cards[0], row)) for row in cards[1:]]
    for i, pack in enumerate(open_packs):
        if pack[0] not in openable:
            open_packs.pop(i)
    for index, pack in enumerate(open_packs):
        for i in range(1, 5):
            try:
                open_packs[index][i] = int(pack[i])
            except ValueError:
                open_packs[index][i] = 0
            except IndexError:
                open_packs[index].append(0)
    pack_counts = [[row[0], int(row[1])] for row in pack_counts]


if __name__ == "__main__":
    get_data()
